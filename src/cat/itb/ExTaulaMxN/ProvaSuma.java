package cat.itb.ExTaulaMxN;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ProvaSuma {

    public static void main(String[] args) {

        int[][] taula = new int[4][4];
        
        for (int i = 0; i < taula.length; i++){
            for (int j = 0; j < taula[i].length;j++){
                taula[i][j] = (int) (Math.random() * 10) + 1;
            }
        }

        for (int i = 0; i < taula.length; i++){
            for (int j = 0; j < taula[i].length;j++){
                System.out.print("["+taula[i][j]+"] ");
            }
            System.out.println();
        }
        
        Taula t1 = new Taula(taula, 0);
        Taula t2 = new Taula(taula, 1);
        Taula t3 = new Taula(taula, 2);
        Taula t4 = new Taula(taula, 3);

        ExecutorService ex = Executors.newFixedThreadPool(4);

        Future<Integer> f1 = ex.submit(t1);
        Future<Integer> f2 = ex.submit(t2);
        Future<Integer> f3 = ex.submit(t3);
        Future<Integer> f4 = ex.submit(t4);

        int i1, i2, i3, i4;
        try {
            i1 = f1.get();
            i2 = f2.get();
            i3 = f3.get();
            i4 = f4.get();
            System.out.println("La primera fila suma: "+i1+"\nLa segona fila suma: "+i2+"\nLa tercera fila suma: "+i3+"\nLa quarta fila suma: "+i4);
            ex.shutdown();
        } catch (InterruptedException | ExecutionException e) {
        }
    }
}
