package cat.itb.ExTaulaMxN;

import java.util.concurrent.Callable;

public class Taula implements Callable<Integer> {
    private int[][] taula;
    private int fila;

    public Taula(int[][] taula, int fila) {
        this.taula = taula;
        this.fila = fila;
    }

    public int sumarFila(){
        int suma = 0;
        for (int j = 0; j < taula[fila].length; j++){
            suma = taula[fila][j] + suma;
        }
        return suma;
    }

    @Override
    public Integer call() throws Exception {
        return sumarFila();
    }
}
